var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");



describe('Testing bicicletas',() => {
    beforeEach( (done) => { 
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB,{ useNewUrlParser:true, useUnifiedTopology: true });
        var db = mongoose.connection;
        db.on('error',console.error.bind(console, 'connection error: '));
        db.once('open',() => {
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach( (done) => { 
        Bicicleta.deleteMany({},function(err,success){
            if(err)console.log(err);
            mongoose.disconnect();
            done();
        });
    });

    describe('Bicicleta.createInstance',() => {
        it('crea una instancia de bicicleta',() => {
            var bici = Bicicleta.createInstance(1,"verde","urbana",[-34.5,-54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis',() => {
        it('comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });      
        });
    });

    describe('Bicicleta.add',() => {
        it('agregamos una bici',(done) => {
            var a = new Bicicleta ({code:1, color:"rojo", modelo:"urbana"});
            Bicicleta.add(a, function(err,newBici){
                if(err)console.log(err);
                Bicicleta.allBicis(function(error, bicis){
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(a.code);
                    done();
                }); 
            });
        });
    });

    describe('Bicicleta.findByCode',() => {
        it('debe devolver bici con id 1',(done) => {
            Bicicleta.allBicis(function(error, bicis){
                expect(bicis.length).toBe(0);

                var a = new Bicicleta ({code:1, color:"rojo", modelo:"urbana"});
                Bicicleta.add(a, function(err,newBici){
                    if(err)console.log(err);

                    var b = new Bicicleta ({code:2, color:"verde", modelo:"urbana"});
                    Bicicleta.add(b, function(err,newBici){
                        if(err)console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toBe(a.code);
                            expect(targetBici.color).toBe(a.color);
                            expect(targetBici.modelo).toBe(a.modelo);

                            done();
                        }); 
                    });
                });
            }); 
        });
    });

    describe('Bicicleta.removeById',() => {
        it('debe eliminar la bici con id 1',(done) => {
            Bicicleta.allBicis(function(error, bicis){
                expect(bicis.length).toBe(0);

                var a = new Bicicleta ({code:1, color:"rojo", modelo:"urbana"});
                Bicicleta.add(a, function(err,newBici){
                    if(err)console.log(err);

                    var b = new Bicicleta ({code:2, color:"verde", modelo:"urbana"});
                    Bicicleta.add(b, function(err,newBici){
                        if(err)console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici){
                            Bicicleta.removeById(targetBici._id, function(error, response){
                                expect(response.deletedCount).toBe(1);
                                done();
                            }); 
                        });
                        
                    });
                });
            }); 
        });
    });

});

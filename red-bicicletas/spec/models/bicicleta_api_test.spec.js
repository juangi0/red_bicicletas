var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");
var server = require("../../bin/www");
var request = require('request');

var baseUrl =  'http://localhost:3000/api/bicicletas';

describe('Bicicletas API',() => {
    beforeEach( (done) => { 
        
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB,{ useNewUrlParser:true, useUnifiedTopology: true });
        var db = mongoose.connection;
        db.on('error',console.error.bind(console, 'connection error: '));
        db.once('open',() => {
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach( (done) => { 
        Bicicleta.deleteMany({},function(err,success){
            if(err)console.log(err);
            mongoose.disconnect();
            done();
        });
    });

    describe('GET BICICLETAS /',() => {
        it('Status code 200', (done) => {
            var a = new Bicicleta ({code:1, color:"rojo", modelo:"urbana"});
            Bicicleta.add(a, function(err,newBici){
                if(err)console.log(err);
                request.get(baseUrl, function(error,response,body){
                    var result = JSON.parse(body);
                    
                    expect(response.statusCode).toBe(200);
                    expect(result.bicicletas.length).toBe(1);
                    done();
                });
            });
            
        });
    });

    describe('POST BICICLETAS /create',() => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"code":10, "color":"verde", "modelo":"urbana", "lat":-34,"lng":-58}';

            request.post({
                headers: headers,
                url: baseUrl +'/create',
                body: aBici
            }, function(error,response,body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe('verde');
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-58);
                done();
            });
        });
    });

    describe('DELETE BICICLETAS /delete',() => {
        it('Status 200', (done) => {
            var a = new Bicicleta ({code:1, color:"rojo", modelo:"urbana"});
            Bicicleta.add(a, function(err1,newBici){
                if(err1)console.log(err1);

                var b = new Bicicleta ({code:2, color:"verde", modelo:"urbana"});
                Bicicleta.add(b, function(err2,newBici){
                    if(err2)console.log(err2);
                    Bicicleta.findByCode(2, function(error, targetBici){
                        var headers = {'content-type': 'application/json'};
                        var idBici = '{"id": "'+targetBici._id+'"}';

                        request.delete({
                            headers: headers,
                            url: baseUrl +'/delete',
                            body: idBici
                        }, function(error,response,body){
                            expect(response.statusCode).toBe(204);
                            done();
                        });
                    }); 
                });
            });

            
        });
    });

});

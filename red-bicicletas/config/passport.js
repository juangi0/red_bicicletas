const passport = require('passport');
const usuarios = require('../controllers/usuarios');
const localStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');

passport.use(new localStrategy(
    function(email, password, done){
        Usuario.findOne({email: email}, function(err, usuario){
            if(err) return done(err);
            if(!usuario) return done(null,false,{message: 'Email no existente o incorrecto'});
            if(!usuario.verificado) return done(null,false,{message: 'Usuario no verificado'});
            if(!usuario.validatePassword(password)) return done(null,false,{message: 'Password incorrecto'});
            return done(null,usuario);
        })
    }
));

passport.serializeUser((user, cb) =>{
    cb(null,user.id);
});

passport.deserializeUser((id, cb) =>{
    Usuario.findById(id, (err,usuario)=>{
        cb(err,usuario);
    });
});

module.exports = passport;
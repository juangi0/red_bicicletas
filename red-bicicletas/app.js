var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
//const passport = require('passport');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');

var Usuario = require('./models/usuario');
var Token = require('./models/token');


var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas');

var authApiRouter = require('./routes/api/auth');
var bicicletasApiRouter = require('./routes/api/bicicletas');
var usuariosApiRouter = require('./routes/api/usuarios');

const store = new session.MemoryStore;
var app = express();

app.set('secretKey','redbicis123');

app.use(session({
  cookie: {maxAge: 240*60*60*1000},
  store: store,
  saveUninitialized: true,
  resave: true,
  secret: 'redbicis'
}));

var mongoose = require('mongoose');
const usuarios = require('./controllers/usuarios');
var mongoDB = 'mongodb://localhost/red_bicicletas';
mongoose.connect(mongoDB,{ useNewUrlParser:true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',console.error.bind(console, 'MongoDb connection error: '));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login',(req,res)=>{
  res.render('session/login');
});

app.post('/login',(req,res,next)=>{
  //passport
  passport.authenticate('local', function(err, usuario, info){
    if(err)return next(err);
    if(!usuario)return res.render('session/login',{info});
    req.logIn(usuario,function(err){
      if(err)return next(err);
      return res.redirect('/');
    });
  })(req,res,next);
});

app.get('/logout',(req,res)=>{
  req.logOut();
  res.redirect('/');
});

app.get('/forgotPassword',(req,res)=>{
  res.render('session/forgotPassword');
});

app.post('/forgotPassword',(req,res)=>{
  Usuario.findOne({email:req.body.email},function(err, usuario){
    if(!usuario)return res.render('session/forgotPassword',{info:{message: 'No existe el email para un usuario existente'}});
    usuario.resetPassword(function(err){
      if(err)return next(err);
      console.log('session/forgotPasswordMessage');
    });
    res.render('session/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token',(req,res, next)=>{
  Token.findOne({token:req.params.token},function(err, token){
    if(!token)return res.status(400).send({type:'not-verified', msg:'No existe un usuario asociado al token. Verifique que su token no haya expirado'});
    
    Usuario.findById(token._userId,function(err,usuario){
      if(!usuario)return res.status(400).send({msg:'No existe un usuario asociado al token'});
      res.render('session/resetPassword', {errors: {}, usuario: usuario});
    });
  });
});

app.post('/resetPassword',(req,res)=>{
  if(req.body.password != req.body.confirm_password){
    res.render('session/resetPassword', {errors: {confirm_password: {message: 'No coinciden los password ingresados'}}, usuario: new Usuario({email: req.body.email})});
    return;
  }

  Usuario.findOne({ email: req.body.email}, function(err, usuario){
    usuario.password = req.body.password;
    usuario.save(function(err){
      if(err) res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})});
      else res.redirect('/login');
    });
  });
});

app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas',loggedIn, bicicletasRouter);

app.use('/api/auth', authApiRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasApiRouter);
app.use('/api/usuarios', usuariosApiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if(req.user){
    next();
  }else{
    console.log('Usuario sin logearse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err,decoded){
    if(err){
      res.json({status:"error", message: err.message, data:null});
    }else{
      req.body.userId = decoded.id;
      console.log('jwt verify: '+ decoded);
      next();
    }
  });
}

module.exports = app;

var express = require('express');
var router = express.Router();
var authController = require('../../controllers/api/authControllerAPI');

router.post('/authenticate',authController.authtenticate);
router.post('/forgotPassword',authController.forgotPassword);


module.exports = router;
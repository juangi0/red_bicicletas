var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var tokenSchema = new Schema({
    token:{
        type: String,
        required: true
    },
    _userId:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Usuario'
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 43200
    }
});


module.exports = mongoose.model('Token',tokenSchema);